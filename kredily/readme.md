This repo is specifically made for backend assesment for kredily - https://bitbucket.org/machax/workspace/snippets/qX54n4/backend-assignment

List of api:
1. login (POST) - api/login
2. get_products(GET) - api/product/?page=<page_no>
3. place_order(POST) - /api/order/
4. order_history(GET) - api/order


API DOC - https://documenter.getpostman.com/view/8840227/2s84Dmw3zN#3dd03625-1c28-4cbc-a7ff-35a6fc7ddbed

Deployment steps on heroku:
1. Create a Procfile in rootfolder and run the wsgi from unicorn
2. create requirements.txt so once code is pushed heroku will install the requirements.txt
3. create runtime.txt in root folder so that heroku understand which python version to use
4. set collectstatic to false in heroku env so that it don't run collectstatic command
5. configure the db with heroku provided db url which will be there in env variable. make sure to use that in settings.py file
6. create a heroku app and init the repo with git.
7. set remote origin to herko provided url so once you push any change it will retrigger the deployment.
8. once server is up. Make sure to run migrations
9. Once migrations are done. Load the data into db by running script in api/scripts.py

For testing purpose use the creds below:
USERNAME - kredily
PASSWORD - password
