from rest_framework import serializers
from api.models import Product, OrderHistory


class ProductSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Product
        fields = '__all__'


class OrderHistorySerializer(serializers.ModelSerializer):
    product_name = serializers.SerializerMethodField()
    product_id = serializers.SerializerMethodField()

    def get_product_name(self, obj):
        return obj.product.name 
    
    def get_product_id(self, obj):
        return obj.product.id 
        
    class Meta:
        model = OrderHistory
        fields = ('product_name','product_id','order_placed_at')

