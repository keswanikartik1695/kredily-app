
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response
from api.models import Product,OrderHistory
from api.serializers import ProductSerializer, OrderHistorySerializer
from rest_framework.views import APIView
from rest_framework import status
from django.db import  transaction
from django.core.paginator import Paginator

PAGE_SIZE = 10

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({'token': token.key},
                    status=HTTP_200_OK)


class ProductApi(APIView ):

    def get(self, request, *args, **kwargs):
        '''
        List all the products in inventory
        '''
        page_number = self.request.GET.get('page',1)
        products = Product.objects.filter(quantity__gt = 0)
        results =  Paginator(products , PAGE_SIZE)
        serializer = ProductSerializer(results.page(page_number), many=True)
        response = {"data":serializer.data,"total_pages":results.num_pages}
        return Response(response, status=status.HTTP_200_OK)
    
class buyProductApi(APIView):

    def get(self, request, pk=None,*args, **kwargs):
        '''
        Purchased history for the user
        '''

        user = request.user
        page_number = self.request.GET.get('page',1)
        order_history = OrderHistory.objects.filter(user=user)
        results =  Paginator(order_history , PAGE_SIZE)
        serializer = OrderHistorySerializer(results.page(page_number), many=True)
        response = {"data":serializer.data,"total_pages":results.num_pages}
        return Response(response, status=status.HTTP_200_OK)
        


    @transaction.atomic
    def post(self, request, pk=None,*args, **kwargs):
        '''
        place an order from the inventory
        '''
        
        print(request.user)
        id = request.data.get('id', None)
        quantity = request.data.get('quantity', None)

        #validation for checking the quantity
        if not quantity:
            return Response({"message":"Please provide the quantity"},status=status.HTTP_400_BAD_REQUEST)
        try:
            product = Product.objects.get(id=id)
        except Product.DoesNotExist:
            return Response({"message":"Sorry product does not exist"},status=status.HTTP_400_BAD_REQUEST)
        
        #validation for checking the quantity against the inventory
        if not (product.quantity >= quantity):
            return Response({"message":"Sorry we have only {} left".format(product.quantity)},status=status.HTTP_400_BAD_REQUEST)

        # transactional call if something goes wrong order should not be placed and inventory should not get updated
        try:
            with transaction.atomic():
                Product.objects.filter(id=id).update(quantity = product.quantity-1)
                OrderHistory.objects.get_or_create(user=request.user, product=product)
        except :
            return Response({"message":"Something went wrong".format(product.quantity)},status=status.HTTP_400_BAD_REQUEST)
        
        return Response({"message":"Order Placed".format(product.quantity)},status=status.HTTP_200_OK)

        
