
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

#

class Product(models.Model):
    name = models.CharField(unique=True,max_length=255)
    price = models.FloatField()
    quantity = models.IntegerField(default=1)
    def __unicode__(self):
        return u'%s' % (self.name)


class OrderHistory(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product= models.ForeignKey(Product, on_delete=models.CASCADE)
    order_placed_at = models.DateTimeField(default=timezone.now, editable=False)

   
