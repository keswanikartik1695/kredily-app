from itertools import product
from api.models import Product

DATA= [{'name': 'beTouch E110', 'price': 4943022, 'quantity': 5},
 {'name': 'beTouch E120', 'price': 5695673, 'quantity': 6},
 {'name': 'beTouch E130', 'price': 4537902, 'quantity': 89},
 {'name': 'beTouch E140', 'price': 587455, 'quantity': 90},
 {'name': 'beTouch E200', 'price': 3258998, 'quantity': 78},
 {'name': 'beTouch E400', 'price': 4492344, 'quantity': 78},
 {'name': 'Liquid', 'price': 3833850, 'quantity': 87},
 {'name': 'Liquid E', 'price': 1209256, 'quantity': 65},
 {'name': 'Liquid Metal', 'price': 5378746, 'quantity': 5},
 {'name': 'neoTouch P300', 'price': 8360563, 'quantity': 5},
 {'name': 'neoTouch P400', 'price': 690023, 'quantity': 6},
 {'name': 'Stream', 'price': 7051027, 'quantity': 89},
 {'name': 'One Touch 701', 'price': 613483, 'quantity': 12},
 {'name': 'One Touch 715', 'price': 4748512, 'quantity': 34},
 {'name': 'OT-980', 'price': 2351991, 'quantity': 45},
 {'name': 'iPhone', 'price': 5343026, 'quantity': 56},
 {'name': 'iPhone 3G', 'price': 3461000, 'quantity': 87},
 {'name': 'iPhone 3GS', 'price': 4943620, 'quantity': 34},
 {'name': 'iPhone 4', 'price': 8791460, 'quantity': 5},
 {'name': 'iPhone 4S', 'price': 4501131, 'quantity': 5},
 {'name': 'Iphone 5', 'price': 2449019, 'quantity': 43},
 {'name': 'C81', 'price': 4789288, 'quantity': 32},
 {'name': 'CF61', 'price': 1183040, 'quantity': 34},
 {'name': 'CL71', 'price': 2946240, 'quantity': 23}]


def bulk_create():
    for i in DATA:
        Product.objects.create(**i)
        