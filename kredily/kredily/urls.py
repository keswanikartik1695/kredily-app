
from django.contrib import admin
from django.urls import include,path
from api import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login/', views.login),
    path('api/product/', views.ProductApi.as_view()),
    path('api/order_history/', views.buyProductApi.as_view()),

]
